﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {

                    Console.WriteLine("Введите длину комнаты (в метрах):");
                    double lengthWall = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите ширину комнаты (в метрах):");
                    double widthWall = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите высоту комнаты (в метрах):");
                    double heightWall = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите длину рулона (в метрах):");
                    double lengthPaper = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите ширину рулона (в метрах):");
                    double widthPaper = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите шаг рисунка рулона (в метрах):");
                    double PictureStep = Convert.ToDouble(Console.ReadLine());
                    int numOf = 0;
                    int numOfPaper = 1;
                    double Piece = 0;
                    double Pieces = 0;
                    if (lengthWall > 0 && widthWall > 0 && heightWall > 0 && lengthPaper > 0 && widthPaper > 0 && PictureStep > 0)
                    {
                        if (lengthPaper < heightWall)
                        {
                            Console.WriteLine("Не советую покупать такие рулоны.");
                            Console.WriteLine("");
                        }
                        else
                        {
                            if (lengthPaper <= PictureStep || heightWall <= PictureStep)
                            {
                                Console.WriteLine("Да у вас рисунка нету. Попробуйте ввести снова.");
                                Console.WriteLine("");
                            }
                            else
                            {
                                double helpLength = lengthPaper;


                                while (numOf <= (int)((2 * (lengthWall + widthWall)) / widthPaper))
                                {
                                    Piece = (((int)(heightWall / PictureStep)) + 1) * PictureStep;
                                    if (lengthPaper > Piece)
                                    {
                                        if (helpLength >= heightWall)
                                        {
                                            Pieces += Piece - heightWall;
                                            numOf++;
                                            helpLength -= Piece;

                                        }
                                        else
                                        {
                                            Pieces += helpLength;
                                            helpLength = lengthPaper;
                                            numOfPaper++;
                                        }

                                    }
                                    else
                                    {
                                        Pieces += helpLength - heightWall;
                                        numOfPaper++;
                                        numOf++;
                                    }
                                    //Console.WriteLine(Piece);
                                    //Console.WriteLine(helpLength);
                                    //Console.WriteLine(Pieces);
                                    //Console.WriteLine(numOfPaper);
                                    //Console.WriteLine(numOf);

                                }
                                Console.WriteLine("Количество рулонов:" + numOfPaper);
                                Console.WriteLine("Количество обрезков:" + Math.Round(Pieces, 3));
                                Console.WriteLine("");
                            }

                        }
                    }

                    else
                    {
                        Console.WriteLine("Ошибка в параметрах. Попробуйте ввести снова.");
                        Console.WriteLine("");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ошибка в параметрах. Попробуйте ввести снова.");
                    Console.WriteLine("");
                }
            }
        }
    }
}
